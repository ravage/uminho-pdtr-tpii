package pdtr.tpii.ers.mi.uminho.messageclient;

import pdtr.tpii.ers.mi.uminho.messagecontract.*;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 28/03/2011
 * Time: 20:38
 * To change this template use File | Settings | File Templates.
 */
public class MessageClient extends Observable implements IMessageServerProxy {
    private String url;
    private IGatekeeper gatekeeper;
    private IMessageServerProxy proxy;
    private final ClientListener clientListener;
    private final Timer timer;

    public MessageClient(String url) throws MalformedURLException, NotBoundException, RemoteException {
        if (System.getSecurityManager() == null)
            System.setSecurityManager(new SecurityManager());

        this.url = url;
        clientListener = new ClientListener();
        timer = new Timer();
    }

    public boolean register(String user, String password) throws MalformedURLException, NotBoundException, RemoteException {
            gatekeeper = getGatekeeper();
            proxy = gatekeeper.register(user, password);

            if (proxy != null) {
                addRemoteListener(clientListener);
                timer.schedule(new PingPongWorker(), 5000, 5000);
            }

            return proxy != null;
    }

    public boolean login(String user, String password) throws MalformedURLException, NotBoundException, RemoteException {
            gatekeeper = getGatekeeper();
            proxy = gatekeeper.login(user, password);

            if (proxy != null) {
                addRemoteListener(clientListener);
                timer.schedule(new PingPongWorker(), 5000, 5000);
            }

            return proxy != null;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isFollowingMe(IUser user) {
        try {
            return proxy.isFollowingMe(user);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void addRemoteListener(IClientListener listener) {
        try {
            IClientListener listenerStub = (IClientListener) UnicastRemoteObject.exportObject(listener, 0);
            proxy.addRemoteListener(listenerStub);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void ping() throws RemoteException {
        proxy.ping();
    }

    private IGatekeeper getGatekeeper() throws MalformedURLException, NotBoundException, RemoteException {
        if (gatekeeper != null)
            return gatekeeper;

        return gatekeeper = (IGatekeeper) Naming.lookup(url);
    }

    @Override
    public void postMessage(String message) {
        assert proxy != null;
        try {
            proxy.postMessage(message);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Iterable<Message> getMessages() {
        assert proxy != null;
        try {
            return proxy.getMessages();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return new ArrayList<Message>();
    }

    @Override
    public boolean follow(IUser user) {
        assert proxy != null;
        try {
            return proxy.follow(user);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean unfollow(IUser user) {
        assert proxy != null;
        try {
            return proxy.unfollow(user);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Iterable<IUser> getFollowers() {
        assert proxy != null;
        try {
            return proxy.getFollowers();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return new ArrayList<IUser>();
    }

    @Override
    public Iterable<IUser> getFollowing() {
        assert proxy != null;
        try {
            return proxy.getFollowing();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return new ArrayList<IUser>();
    }

    @Override
    public Iterable<IUser> getOthers() {
        assert proxy != null;
        try {
            return proxy.getOthers();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return new ArrayList<IUser>();
    }

    private class ClientListener implements IClientListener {
        @Override
        public synchronized void follow(String nickname) throws RemoteException {
            setChanged();
            notifyObservers(new RemoteStateChanged<String>(ClientListenerState.FOLLOW, nickname));
        }

        @Override
        public synchronized void unfollow(String nickname) throws RemoteException {
            setChanged();
            notifyObservers(new RemoteStateChanged<String>(ClientListenerState.UNFOLLOW, nickname));
        }

        @Override
        public synchronized void messages() throws RemoteException {
            setChanged();
            notifyObservers(new RemoteStateChanged<String>(ClientListenerState.MESSAGE, null));
        }

        @Override
        public void pong() throws RemoteException {
            System.out.println("PONG");
        }
    }

    private class PingPongWorker extends TimerTask {
        @Override
        public void run() {
            try {
                proxy.ping();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
}
