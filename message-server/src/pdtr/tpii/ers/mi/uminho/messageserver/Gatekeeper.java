package pdtr.tpii.ers.mi.uminho.messageserver;

import pdtr.tpii.ers.mi.uminho.messagecontract.*;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.rmi.server.RemoteServer;
import java.rmi.server.ServerNotActiveException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 30/03/2011
 * Time: 02:29
 * To change this template use File | Settings | File Templates.
 */
public class Gatekeeper implements IGatekeeper {
    private transient final IMessageServer server;

    public Gatekeeper(IMessageServer server) {
        this.server = server;
    }

    @Override
    public IMessageServerProxy login(String nickname, String password) throws RemoteException, SecurityException {
        String host = "0.0.0.0";
        Digest digest = Digest.INSTANCE;

        try {
            host = RemoteServer.getClientHost();
        } catch (ServerNotActiveException e) {
            e.printStackTrace();
        }

        IUser user = server.login(nickname, digest.toSHA1(password), host);
        return new MessageServerProxy(user, server);
    }

    @Override
    public IMessageServerProxy register(String nickname, String password) throws RemoteException, SecurityException {
        String host = "0.0.0.0";
        Digest digest = Digest.INSTANCE;
        try {
            host = RemoteServer.getClientHost();
        } catch (ServerNotActiveException e) {
            e.printStackTrace();
        }

        IUser user = server.register(nickname, digest.toSHA1(password), host);
        return new MessageServerProxy(user, server);
    }
}
