package pdtr.tpii.ers.mi.uminho.messageserver;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 29/03/2011
 * Time: 17:30
 * To change this template use File | Settings | File Templates.
 */
public class Dispatcher implements Runnable {
    private final ConcurrentLinkedQueue<IListener> listeners;
    private final ConcurrentLinkedQueue<IEventAdapter> events;

    public Dispatcher() {
        listeners = new ConcurrentLinkedQueue<IListener>();
        events = new ConcurrentLinkedQueue<IEventAdapter>();
    }

    public void addEventListener(IListener listener) {
        listeners.add(listener);
    }

    public void removeEventListener(IListener listener) {
        listeners.remove(listener);
    }

    public void addEvent(IEventAdapter event) {
        events.add(event);
        synchronized (this) {
            this.notify();
        }
    }

    public void removeEvent(IEventAdapter event) {
        events.remove(event);
    }

    private synchronized void dispatch() {
        for (IListener listener : listeners) {
            for (IEventAdapter eventAdapter : events) {
                eventAdapter.dispatch(listener);
            }
        }
        events.clear();
    }

    @Override
    public void run() {
        Thread.currentThread().setName("Dispatcher-Thread");
        while (!Thread.interrupted()) {
            try {
                synchronized (this) {
                    while(events.isEmpty())
                        wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            dispatch();
        }
    }
}
