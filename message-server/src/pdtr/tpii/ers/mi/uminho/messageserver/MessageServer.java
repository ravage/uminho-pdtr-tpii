package pdtr.tpii.ers.mi.uminho.messageserver;

import com.almworks.sqlite4java.SQLiteException;
import pdtr.tpii.ers.mi.uminho.messagecontract.*;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 28/03/2011
 * Time: 19:27
 * To change this template use File | Settings | File Templates.
 */
public class MessageServer implements IMessageServer {
    private final Dispatcher dispatcher;
    private final SessionManager session;
    private IUserModel model;
    private final Executor executor;

    public MessageServer() {
        dispatcher = new Dispatcher();
        session = SessionManager.INSTANCE;
        executor = Executors.newSingleThreadExecutor();
        try {
            model = new UserModel();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public void start() throws RemoteException {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }

        Registry registry = LocateRegistry.createRegistry(2045);

        IMessageServer serverStub = (IMessageServer) UnicastRemoteObject.exportObject(this, 0);
        Gatekeeper gatekeeper = new Gatekeeper(serverStub);
        IGatekeeper cerberus = (IGatekeeper)UnicastRemoteObject.exportObject(gatekeeper, 0);
        executor.execute(dispatcher);
        registry.rebind("MessageServer", cerberus);
    }

    @Override
    public void postMessage(final String token, final String message) throws RemoteException {
        session.isRegistered(token);
        final IUser issuer = session.getUser(token);

        model.post(issuer, message);

        dispatcher.addEvent(new IEventAdapter() {
            @Override
            public void dispatch(IListener listener) {
                listener.messagePosted(issuer, message);
            }
        });

        clientCallbackMessages(issuer);
    }

    private void clientCallbackMessages(IUser issuer) {
        List<String> followers = model.getConnectedFollowers(issuer);
        for (String token : followers) {
            User user = session.getUser(token);
            if (user != null) {
                try {
                    user.getListener().messages();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public Iterable<Message> getMessages(String token) throws RemoteException {
        session.isRegistered(token);
        final IUser issuer = session.getUser(token);

        dispatcher.addEvent(new IEventAdapter() {
            @Override
            public void dispatch(IListener listener) {
                listener.getMessagesRequest(issuer);
            }
        });

        return model.getMessages(issuer);
    }

    @Override
    public boolean follow(String token, final IUser user) throws RemoteException {
        session.isRegistered(token);
        final IUser issuer = session.getUser(token);
        boolean followResult = model.follow(issuer, user);

        if (!followResult)
            return false;

        dispatcher.addEvent(new IEventAdapter() {
            @Override
            public void dispatch(IListener listener) {
                listener.followRequest(issuer, user);
            }
        });

        clientCallbackFollow(issuer, user);

        return followResult;
    }

    private void clientCallbackFollow(IUser issuer,  IUser user) {
        User followed = session.getUser(model.getToken(user.getName()));
        if (followed != null) {
            try {
                followed.getListener().follow(issuer.getName());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean unfollow(String token, final IUser user) throws RemoteException {
        session.isRegistered(token);
        final IUser issuer = session.getUser(token);

        dispatcher.addEvent(new IEventAdapter() {
            @Override
            public void dispatch(IListener listener) {
                listener.unfollowRequest(issuer, user);
            }
        });

        clientCallbackUnfollow(issuer, user);

        return model.unfollow(issuer, user);
    }

    private void clientCallbackUnfollow(IUser issuer, IUser user) {
        String token = model.getToken(user.getName());

        if (token == null)
            return;

        User unfollowed = session.getUser(token);
        if (unfollowed != null) {
            try {
                unfollowed.getListener().unfollow(issuer.getName());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public IUser register(final String nickname, String password, final String host) throws RemoteException {
        final boolean state = model.register(nickname, password, session.getToken());
        final User issuer = new User(nickname, password, session.getToken(), host);

        dispatcher.addEvent(new IEventAdapter() {
            @Override
            public void dispatch(IListener listener) {
                listener.userRegistered(nickname, host, state);
            }
        });

        if (state)
            session.registerSession(issuer);
        else
            throw new SecurityException("User already exists!");

        issuer.setId(model.getId(issuer.getName()));

        return issuer;
    }

    @Override
    public IUser login(final String nickname, final String password, final String host) throws RemoteException {
        final String token = session.getToken();
        final boolean loginResult = model.login(nickname, password, token);

        dispatcher.addEvent(new IEventAdapter() {
            @Override
            public void dispatch(IListener listener) {
                listener.userLoggedIn(nickname, host, loginResult);
            }
        });

        if (!loginResult)
            throw new SecurityException("Invalid login credentials!");

        final User issuer = new User(nickname, password, token, host);
        issuer.setId(model.getId(nickname));

        session.registerSession(issuer);

        return issuer;
    }

    @Override
    public Iterable<IUser> getFollowers(String token) throws RemoteException {
        session.isRegistered(token);
        final IUser issuer = session.getUser(token);

        dispatcher.addEvent(new IEventAdapter() {
            @Override
            public void dispatch(IListener listener) {
                listener.userListRequest(issuer);
            }
        });

        return model.getFollowers(issuer);
    }

    @Override
    public Iterable<IUser> getFollowing(String token) throws RemoteException {
        session.isRegistered(token);
        final IUser issuer = session.getUser(token);

        dispatcher.addEvent(new IEventAdapter() {
            @Override
            public void dispatch(IListener listener) {
                listener.userListRequest(issuer);
            }
        });

        return model.getFollowing(issuer);
    }

    @Override
    public Iterable<IUser> getOthers(String token) throws RemoteException {
        session.isRegistered(token);
        final IUser issuer = session.getUser(token);

        dispatcher.addEvent(new IEventAdapter() {
            @Override
            public void dispatch(IListener listener) {
                listener.userListRequest(issuer);
            }
        });

        return model.getOthers(issuer);
    }

    @Override
    public boolean isFollowingMe(String token, IUser user) throws RemoteException {
        session.isRegistered(token);
        final User requester = session.getUser(token);
        return model.isFollowingMe(requester, user);
    }

    @Override
    public void addClientListener(String token, IClientListener listener) {
        session.isRegistered(token);
        final User issuer = session.getUser(token);
        issuer.attachListener(listener);
    }

    @Override
    public void ping(String token) throws RemoteException {
        session.isRegistered(token);
        final User issuer =  session.getUser(token);
        session.checkIn(token);
        issuer.getListener().pong();

        dispatcher.addEvent(new IEventAdapter() {
            @Override
            public void dispatch(IListener listener) {
                listener.pingNotification(issuer);
            }
        });

    }


    public void addEventListener(IListener listener) {
        dispatcher.addEventListener(listener);
    }


}
