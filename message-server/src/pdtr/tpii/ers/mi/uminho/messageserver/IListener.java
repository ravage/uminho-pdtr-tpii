package pdtr.tpii.ers.mi.uminho.messageserver;

import pdtr.tpii.ers.mi.uminho.messagecontract.IUser;
import pdtr.tpii.ers.mi.uminho.messagecontract.User;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 29/03/2011
 * Time: 17:38
 * To change this template use File | Settings | File Templates.
 */
public interface IListener {
    void userRegistered(String nickname, String host, boolean state);

    void followRequest(IUser requester, IUser user);

    void unfollowRequest(IUser requester, IUser user);

    void messagePosted(IUser requester, String message);

    void userLoggedIn(String nickname, String host, boolean b);

    void userListRequest(IUser issuer);

    void getMessagesRequest(IUser issuer);

    void pingNotification(User issuer);
}
