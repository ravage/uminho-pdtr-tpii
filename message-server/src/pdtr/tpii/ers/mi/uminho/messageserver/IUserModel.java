package pdtr.tpii.ers.mi.uminho.messageserver;

import pdtr.tpii.ers.mi.uminho.messagecontract.IUser;
import pdtr.tpii.ers.mi.uminho.messagecontract.Message;

import java.util.List;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 30/03/2011
 * Time: 15:58
 * To change this template use File | Settings | File Templates.
 */
public interface IUserModel {
    boolean register(String nickname, String password, String token);

    boolean login(String nickname, String password, String token);

    boolean unfollow(IUser issuer, IUser user);

    boolean follow(IUser issuer, IUser user);

    Iterable<IUser> getFollowing(IUser issuer);

    Iterable<IUser> getFollowers(IUser issuer);

    Iterable<IUser> getOthers(IUser issuer);

    Iterable<Message> getMessages(IUser issuer);

    void post(IUser issuer, String message);

    int getId(String nickname);

    boolean isFollowingMe(IUser issuer, IUser user);

    List<String> getConnectedFollowers(IUser issuer);

    String getToken(String name);
}
