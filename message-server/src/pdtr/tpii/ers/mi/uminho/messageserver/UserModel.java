package pdtr.tpii.ers.mi.uminho.messageserver;

import com.almworks.sqlite4java.*;
import pdtr.tpii.ers.mi.uminho.messagecontract.IUser;
import pdtr.tpii.ers.mi.uminho.messagecontract.Message;
import pdtr.tpii.ers.mi.uminho.messagecontract.User;

import java.io.File;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 30/03/2011
 * Time: 16:05
 * To change this template use File | Settings | File Templates.
 */
public class UserModel implements IUserModel {
    private final SQLiteQueue queue;

    public UserModel() throws SQLiteException {
        queue = new SQLiteQueue(new File("message-server/data/db.sq3"));
        queue.start();
    }

    @Override
    public boolean register(final String nickname, final String password, final String token) {
        if (userExists(nickname))
            return false;

        queue.execute(new SQLiteJob<Object>() {
            @Override
            protected Object job(SQLiteConnection connection) throws Throwable {
                SQLiteStatement stmt = connection.prepare("INSERT INTO users (nickname, password, token, connected) VALUES (?, ?, ?, ?)");
                try {
                    stmt.bind(1, nickname);
                    stmt.bind(2, password);
                    stmt.bind(3, token);
                    stmt.bind(4, 1);
                    stmt.step();
                } finally {
                    stmt.dispose();
                }
                return null;
            }
        }).complete();

        return true;
    }

    @Override
    public boolean login(final String nickname, final String password, String token) {
        boolean result = queue.execute(new SQLiteJob<Boolean>() {
            @Override
            protected Boolean job(SQLiteConnection connection) throws Throwable {
                SQLiteStatement stmt = connection.prepare("SELECT COUNT(*) FROM users WHERE nickname = ? AND password = ?");
                try {
                    stmt.bind(1, nickname);
                    stmt.bind(2, password);
                    stmt.step();

                    return stmt.columnInt(0) == 1;
                }  finally {
                    stmt.dispose();
                }
            }
        }).complete();

        if (result)
            touchLogin(nickname, token);

        return result;
    }

    @Override
    public boolean unfollow(final IUser issuer, IUser user) {
        final User requester = (User)issuer;
        final User other = (User)user;
        other.setId(getId(other.getName()));

        queue.execute(new SQLiteJob<Object>() {
            @Override
            protected Object job(SQLiteConnection connection) throws Throwable {
                SQLiteStatement stmt = connection.prepare("DELETE FROM connections WHERE user_id = ? AND other_id = ?");
                try {
                    stmt.bind(1, requester.getId());
                    stmt.bind(2, other.getId());
                    stmt.step();
                    return null;
                } finally {
                    stmt.dispose();
                }
            }
        }).complete();
        return true;
    }

    @Override
    public boolean follow(IUser issuer, IUser user) {
        final User requester = (User)issuer;
        final User other = (User)user;
        other.setId(getId(other.getName()));

        if (!userExists(user.getName()) || isFollowingMe(other, requester))
            return false;

        queue.execute(new SQLiteJob<Object>() {
            @Override
            protected Object job(SQLiteConnection connection) throws Throwable {
                SQLiteStatement stmt = connection.prepare("INSERT INTO connections (user_id, other_id) VALUES (?, ?)");
                try {
                    stmt.bind(1, requester.getId());
                    stmt.bind(2, other.getId());
                    stmt.step();
                    return null;
                }  finally {
                    stmt.dispose();
                }
            }
        }).complete();
        return true;
    }

    @Override
    public Iterable<IUser> getFollowing(final IUser issuer) {
        final List<IUser> following = new ArrayList<IUser>();
        final User requester = (User)issuer;

        return queue.execute(new SQLiteJob<List<IUser>>() {
            @Override
            protected List<IUser> job(SQLiteConnection connection) throws Throwable {
                StringBuilder sb = new StringBuilder();
                sb.append("SELECT u.id, u.nickname FROM users u ");
                sb.append("JOIN connections c ON u.id = c.other_id ");
                sb.append("WHERE c.user_id = ?");

                SQLiteStatement stmt = connection.prepare(sb.toString());

                try {
                    stmt.bind(1, requester.getId());
                    while(stmt.step())
                        following.add(new User(stmt.columnInt(0), stmt.columnString(1)));

                    return following;
                } finally {
                    stmt.dispose();
                }
            }
        }).complete();
    }

    @Override
    public Iterable<IUser> getFollowers(IUser issuer) {
        final User requester = (User)issuer;
        final List<IUser> users = new ArrayList<IUser>();
        return queue.execute(new SQLiteJob<List<IUser>>() {
            @Override
            protected List<IUser> job(SQLiteConnection connection) throws Throwable {
                StringBuilder sql = new StringBuilder();
                sql.append("SELECT u.id, u.nickname FROM users u ");
                sql.append("JOIN connections c ON u.id = c.user_id ");
                sql.append("WHERE c.other_id = ?");
                SQLiteStatement stmt = connection.prepare(sql.toString());
                try {
                    stmt.bind(1, requester.getId());
                    while (stmt.step())
                        users.add(new User(stmt.columnInt(0), stmt.columnString(1)));

                    return users;
                } finally {
                    stmt.dispose();
                }
            }
        }).complete();
    }

    @Override
    public Iterable<IUser> getOthers(final IUser issuer) {
        final User requester = (User)issuer;
        final List<IUser> users = new ArrayList<IUser>();
        return queue.execute(new SQLiteJob<List<IUser>>() {
            @Override
            protected List<IUser> job(SQLiteConnection connection) throws Throwable {
                StringBuilder sql = new StringBuilder();
                sql.append("SELECT id, nickname FROM users ");
                sql.append("WHERE id NOT IN ");
                sql.append("(SELECT user_id FROM connections WHERE user_id = ? OR other_id = ?) ");
                sql.append("AND id NOT IN ");
                sql.append("(SELECT other_id FROM connections WHERE user_id = ? OR other_id = ?) ");
                SQLiteStatement stmt = connection.prepare(sql.toString());
                try {
                    stmt.bind(1, requester.getId());
                    stmt.bind(2, requester.getId());
                    stmt.bind(3, requester.getId());
                    stmt.bind(4, requester.getId());
                    while (stmt.step())
                       users.add(new User(stmt.columnInt(0), stmt.columnString(1)));

                    return users;
                } finally {
                    stmt.dispose();
                }
            }
        }).complete();
    }

    @Override
    public Iterable<Message> getMessages(IUser issuer) {
        final List<Message> messages = new ArrayList<Message>();
        final User requester = (User)issuer;
        boolean result = queue.execute(new SQLiteJob<Boolean>() {
            @Override
            protected Boolean job(SQLiteConnection connection) throws Throwable {
                StringBuilder sql = new StringBuilder();
                sql.append("SELECT u.nickname, m.message FROM users u ");
                sql.append("JOIN messages m ON u.id = m.user_id ");
                sql.append("WHERE m.user_id IN (SELECT other_id FROM connections WHERE user_id = ?)");
                sql.append("AND m.inserted_at >= (SELECT last_read_request from users WHERE id = ?)");
                SQLiteStatement stmt = connection.prepare(sql.toString());
                try {
                    stmt.bind(1, requester.getId());
                    stmt.bind(2, requester.getId());

                    while(stmt.step())
                        messages.add(new Message(stmt.columnString(0), stmt.columnString(1)));

                    return true;
                } finally {
                    stmt.dispose();
                }
            }
        }).complete();

        if (result)
            touchReadRequest(issuer.getName());

        return messages;
    }

    @Override
    public void post(IUser issuer, final String message) {
        final User user = (User)issuer;
        queue.execute(new SQLiteJob<Object>() {
            @Override
            protected Object job(SQLiteConnection connection) throws Throwable {
                SQLiteStatement stmt = connection.prepare("INSERT INTO messages (user_id, message) VALUES (?, ?)");
                try {
                    stmt.bind(1, user.getId());
                    stmt.bind(2, message);
                    stmt.step();
                    return null;
                }  finally {
                    stmt.dispose();
                }
            }
        }).complete();
    }

    private boolean userExists(final String nickname) {
        return queue.execute(new SQLiteJob<Boolean>() {
            @Override
            protected Boolean job(SQLiteConnection connection) throws Throwable {
                SQLiteStatement stmt = connection.prepare("SELECT COUNT(*) FROM users WHERE nickname = ?");
                try {
                    stmt.bind(1, nickname);
                    stmt.step();
                    return  stmt.columnInt(0) == 1;
                } finally {
                    stmt.dispose();
                }
            }
        }).complete();
    }

    private void touchLogin(final String nickname, final String token) {
        queue.execute(new SQLiteJob<Object>() {
            @Override
            protected Object job(SQLiteConnection connection) throws Throwable {
                String sql = "UPDATE users SET updated_at = CURRENT_TIMESTAMP, token = ? WHERE nickname = ?";
                SQLiteStatement stmt = connection.prepare(sql);
                try {
                    stmt.bind(1, token);
                    stmt.bind(2, nickname);
                    stmt.step();
                    return null;
                }  finally {
                    stmt.dispose();
                }
            }
        }).complete();
    }

    // FIXME: shoud be database trigger
    private void touchReadRequest(final String nickname) {
        queue.execute(new SQLiteJob<Object>() {
            @Override
            protected Object job(SQLiteConnection connection) throws Throwable {
                String sql = "UPDATE users SET last_read_request = CURRENT_TIMESTAMP WHERE nickname = ?";
                SQLiteStatement stmt = connection.prepare(sql);
                try {
                    stmt.bind(1, nickname);
                    stmt.step();
                    return null;
                }  finally {
                    stmt.dispose();
                }
            }
        }).complete();
    }

    public int getId(final String nickname) {
        return queue.execute(new SQLiteJob<Integer>() {
            @Override
            protected Integer job(SQLiteConnection connection) throws Throwable {
                SQLiteStatement stmt = connection.prepare("SELECT id FROM users WHERE nickname = ?");
                try {
                    stmt.bind(1, nickname);
                    stmt.step();
                    return stmt.columnInt(0);
               } finally {
                    stmt.dispose();
                }
            }
        }).complete();
    }

    @Override
    public boolean isFollowingMe(IUser issuer, IUser user) {
        final User requester = (User)issuer;
        final User other = (User)user;
        other.setId(getId(other.getName()));

        return queue.execute(new SQLiteJob<Boolean>() {
            @Override
            protected Boolean job(SQLiteConnection connection) throws Throwable {
                StringBuilder sql = new StringBuilder();
                sql.append("SELECT COUNT(*) FROM connections ");
                sql.append("WHERE user_id = ? AND other_id = ?");
                SQLiteStatement stmt = connection.prepare(sql.toString());
                try {
                    stmt.bind(1, other.getId());
                    stmt.bind(2, requester.getId());
                    stmt.step();
                    return stmt.columnInt(0) >= 1;
                }   finally {
                    stmt.dispose();
                }
            }
        }).complete();
    }

    @Override
    public List<String> getConnectedFollowers(IUser issuer) {
        final User requester = (User)issuer;
        final List<String> followers = new ArrayList<String>();
        return queue.execute(new SQLiteJob<List<String>>() {
            @Override
            protected List<String> job(SQLiteConnection connection) throws Throwable {
                StringBuilder sql = new StringBuilder();
                sql.append("SELECT u.token FROM users u ");
                sql.append("JOIN connections c ON u.id = c.user_id ");
                sql.append("WHERE c.other_id = ? AND u.connected = 1");
                SQLiteStatement stmt = connection.prepare(sql.toString());
                try {
                    stmt.bind(1, requester.getId());
                    while (stmt.step())
                        followers.add(stmt.columnString(0));

                    return followers;
                } finally {
                    stmt.dispose();
                }
            }
        }).complete();
    }

    @Override
    public String getToken(final String name) {
        return queue.execute(new SQLiteJob<String>() {
            @Override
            protected String job(SQLiteConnection connection) throws Throwable {
                SQLiteStatement stmt = connection.prepare("SELECT token FROM users WHERE nickname = ?");
                try {
                    stmt.bind(1, name);

                    if (stmt.step())
                        return stmt.columnString(0);

                    return null;
                } finally {
                    stmt.dispose();
                }
            }
        }).complete();
    }
}
