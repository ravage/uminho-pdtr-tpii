package pdtr.tpii.ers.mi.uminho.messageserver;

import pdtr.tpii.ers.mi.uminho.messagecontract.IUser;
import pdtr.tpii.ers.mi.uminho.messagecontract.User;

import java.rmi.RemoteException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 29/03/2011
 * Time: 18:11
 * To change this template use File | Settings | File Templates.
 */
public enum SessionManager {
    INSTANCE;

    private final ConcurrentHashMap<String, User> session = new ConcurrentHashMap<String, User>();
    private final ConcurrentHashMap<String, Date> times = new ConcurrentHashMap<String, Date>();

    public void registerSession(User user) {
        session.putIfAbsent(user.getToken(), user);
    }

    public String getToken() {
        return UUID.randomUUID().toString();
    }

    User getUser(String token) {
        return session.get(token);
    }

    /*User getUser(int id) {
        for (User user : session.values())
            if (user.getId() == id)
                return user;

        return null;
    }*/

    public void isRegistered(String token) throws SecurityException {
        if (!session.containsKey(token))
            throw new SecurityException("Session expired!");
    }

    public void checkIn(String token) {
        times.put(token, new Date());
        purge();
    }

    private void purge() {
        Iterator<Map.Entry<String, Date>> iterator = times.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Date> item = iterator.next();

            if (item.getValue().getTime() + 20 * 1000 > new Date().getTime())
                continue;

            session.remove(item.getKey());
            iterator.remove();
        }
    }
}
