package pdtr.tpii.ers.mi.uminho.messageserver;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 29/03/2011
 * Time: 17:40
 * To change this template use File | Settings | File Templates.
 */
public interface IEventAdapter {
    void dispatch(IListener listener);
}
