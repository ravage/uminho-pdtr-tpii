package pdtr.tpii.ers.mi.uminho.messagecontract;

import java.awt.event.ItemListener;
import java.rmi.RemoteException;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 30/03/2011
 * Time: 02:34
 * To change this template use File | Settings | File Templates.
 */
public class MessageServerProxy implements IMessageServerProxy {
    private final IUser issuer;
    private final IMessageServer server;

    public MessageServerProxy(IUser user,  IMessageServer server) {
        issuer = user;
        this.server = server;
    }

    @Override
    public void postMessage(String message) throws RemoteException {
        server.postMessage(issuer.getToken(), message);
    }

    @Override
    public Iterable<Message> getMessages() throws RemoteException {
        return server.getMessages(issuer.getToken());
    }

    @Override
    public boolean follow(IUser user) throws RemoteException {
        return server.follow(issuer.getToken(),  user);
    }

    @Override
    public boolean unfollow(IUser user) throws RemoteException {
        return server.unfollow(issuer.getToken(), user);
    }

    @Override
    public Iterable<IUser> getFollowers() throws RemoteException {
        return server.getFollowers(issuer.getToken());
    }

    @Override
    public Iterable<IUser> getFollowing() throws RemoteException {
        return server.getFollowing(issuer.getToken());
    }

    @Override
    public Iterable<IUser> getOthers() throws RemoteException {
        return server.getOthers(issuer.getToken());
    }

    @Override
    public boolean isFollowingMe(IUser user) throws RemoteException {
        return server.isFollowingMe(issuer.getToken(), user);
    }

    @Override
    public void addRemoteListener(IClientListener listener) throws RemoteException {
        server.addClientListener(issuer.getToken(), listener);
    }

    @Override
    public void ping() throws RemoteException {
        server.ping(issuer.getToken());
    }
}
