package pdtr.tpii.ers.mi.uminho.messagecontract;

import pdtr.tpii.ers.mi.uminho.messagecontract.IClientListener;

import java.rmi.RemoteException;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 02/04/2011
 * Time: 17:24
 * To change this template use File | Settings | File Templates.
 */
public class NullClientListener implements IClientListener {
    @Override
    public void follow(String nickname) throws RemoteException {}

    @Override
    public void unfollow(String nickname) throws RemoteException {}

    @Override
    public void messages() throws RemoteException {}

    @Override
    public void pong() throws RemoteException {}
}
