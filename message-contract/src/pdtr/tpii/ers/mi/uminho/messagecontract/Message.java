package pdtr.tpii.ers.mi.uminho.messagecontract;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 01/04/2011
 * Time: 18:54
 * To change this template use File | Settings | File Templates.
 */
public class Message implements Serializable {
    private final String nickname;
    private final String message;

    public Message(String nickname, String message) {
        this.nickname = nickname;
        this.message = message;
    }

    public String getNickname() {
        return nickname;
    }

    public String getMessage() {
        return message;
    }
}
