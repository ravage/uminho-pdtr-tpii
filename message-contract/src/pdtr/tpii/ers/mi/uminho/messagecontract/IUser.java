package pdtr.tpii.ers.mi.uminho.messagecontract;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 28/03/2011
 * Time: 18:33
 * To change this template use File | Settings | File Templates.
 */
public interface IUser extends Serializable   {
    // get the user name
    public String getName();

    // get the user password
    public String getPassword();

    // session token
    public String getToken();

    // user host
    public String getHost();
}
