package pdtr.tpii.ers.mi.uminho.messagecontract;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 30/03/2011
 * Time: 02:30
 * To change this template use File | Settings | File Templates.
 */
public interface IGatekeeper extends Remote {
    public IMessageServerProxy login(String nickname, String password) throws RemoteException, SecurityException;
    public IMessageServerProxy register(String nickname, String password) throws RemoteException, SecurityException;
}
