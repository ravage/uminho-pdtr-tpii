package pdtr.tpii.ers.mi.uminho.messagecontract;
import pdtr.tpii.ers.mi.uminho.messagecontract.IUser;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 30/03/2011
 * Time: 12:17
 * To change this template use File | Settings | File Templates.
 */
public interface IMessageServer extends Remote {
    // store a message on the server
    public void postMessage(String token, String message) throws RemoteException;
    // get the messages stored for the evocating user
    public Iterable<Message> getMessages(String token) throws RemoteException;
    // follow a given user
    public boolean follow(String token, IUser user) throws RemoteException;
    // unfollow  a given user
    public boolean unfollow(String token, IUser user) throws RemoteException;
    // register the user
    public IUser register(String nickname, String password, String host) throws RemoteException;
    // user login
    public IUser login(String nickname, String password, String host) throws RemoteException;
    // get followers
    public Iterable<IUser> getFollowers(String token) throws RemoteException;
    // get following
    public Iterable<IUser> getFollowing(String token) throws RemoteException;
    // get all others
    public Iterable<IUser> getOthers(String token) throws RemoteException;
    boolean isFollowingMe(String token, IUser user) throws RemoteException;
    void addClientListener(String token, IClientListener listener) throws RemoteException;
    void ping(String token) throws RemoteException;
}
