package pdtr.tpii.ers.mi.uminho.messagecontract;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 02/04/2011
 * Time: 17:03
 * To change this template use File | Settings | File Templates.
 */
public interface IClientListener extends Remote {
    void follow(String nickname) throws RemoteException;
    void unfollow(String nickname) throws RemoteException;
    void messages() throws RemoteException;
    void pong() throws RemoteException;
}
