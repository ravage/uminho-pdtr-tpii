package pdtr.tpii.ers.mi.uminho.messagecontract;

import java.io.Serializable;
import java.rmi.RemoteException;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 30/03/2011
 * Time: 15:27
 * To change this template use File | Settings | File Templates.
 */
public interface IMessageServerProxy extends Serializable {
    // store a message on the server
    public void postMessage(String message) throws RemoteException;
    // get the messages stored for the evocating user
    public Iterable<Message> getMessages() throws RemoteException;
    // follow a given user
    public boolean follow(IUser user) throws RemoteException;
    // unfollow  a given user
    public boolean unfollow(IUser user) throws RemoteException;
    // get followers
    public Iterable<IUser> getFollowers() throws RemoteException;
    // get following
    public Iterable<IUser> getFollowing() throws RemoteException;
    // get all others
    public Iterable<IUser> getOthers() throws RemoteException;
    boolean isFollowingMe(IUser user) throws RemoteException;
    void addRemoteListener(IClientListener listener) throws RemoteException;
    void ping() throws RemoteException;
}
