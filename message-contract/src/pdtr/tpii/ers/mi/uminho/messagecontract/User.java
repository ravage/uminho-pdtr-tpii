package pdtr.tpii.ers.mi.uminho.messagecontract;

import pdtr.tpii.ers.mi.uminho.messagecontract.IUser;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 28/03/2011
 * Time: 19:06
 * To change this template use File | Settings | File Templates.
 */
public class User implements IUser {
    private String nickname;
    private transient String password;
    private transient int id;
    private transient IClientListener listener;
    private String token;
    private String host;

    public User(String nickname, String password, String token, String host) {
        this.nickname = nickname;
        this.password = password;
        this.token = token;
        this.host = host;
        this.listener = new NullClientListener();
    }

    public User(int id, String nickname) {
        this.id = id;
        this.nickname = nickname;
    }

    public synchronized int getId() {
        return id;
    }

    public synchronized void setId(int value) {
        id = value;
    }

    @Override
    public synchronized String getName() {
        return nickname;
    }

    @Override
    public synchronized String getPassword() {
        return password;
    }

    @Override
    public synchronized String getHost() {
        return host;
    }

    @Override
    public synchronized String getToken() {
        return token;
    }

    @Override
    public String toString() {
        return getName();
    }

    public synchronized void attachListener(IClientListener listener) {
        this.listener = listener;
    }

    public synchronized IClientListener getListener() {
        return listener;
    }
}
