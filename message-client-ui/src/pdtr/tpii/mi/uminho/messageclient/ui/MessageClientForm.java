package pdtr.tpii.mi.uminho.messageclient.ui;

import pdtr.tpii.ers.mi.uminho.messageclient.ClientListenerState;
import pdtr.tpii.ers.mi.uminho.messageclient.ILoginListener;
import pdtr.tpii.ers.mi.uminho.messageclient.MessageClient;
import pdtr.tpii.ers.mi.uminho.messageclient.RemoteStateChanged;
import pdtr.tpii.ers.mi.uminho.messagecontract.IUser;
import pdtr.tpii.ers.mi.uminho.messagecontract.Message;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import static javax.swing.ListSelectionModel.SINGLE_SELECTION;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 29/03/2011
 * Time: 15:31
 * To change this template use File | Settings | File Templates.
 */
public class MessageClientForm implements Runnable {
    private JPanel panel;
    private JTable tblFollowing;
    private JButton btnUnfollow;
    private JTable tblFollowers;
    private JButton btnFollow;
    private JTable tblOthers;
    private JButton btnFollowOthers;
    private JTextArea tblMessages;
    private JTextArea txtMessage;
    private JButton btnSend;
    private JSplitPane spLeft;
    private JSplitPane spBase;
    private JPanel panelFollowing;
    private JPanel panelLeftBottom;
    private JButton btnLogin;
    private JButton btnRegister;
    private JTextField txtUrl;
    private JButton btnLogout;
    private JTabbedPane tabPane;
    private JButton btnGetMessages;
    private JTextArea txtMessages;
    private JScrollPane jspMessages;
    private JScrollPane jspMessage;
    private JLabel lblNotification;
    private JFrame frame;

    private final DefaultTableModel modelFollowing;
    private final DefaultTableModel modelFollowers;
    private final DefaultTableModel modelOthers;

    private final MessageClient client;
    private final SimpleDateFormat dateFormat;

    public MessageClientForm() throws MalformedURLException, NotBoundException, RemoteException {
        modelFollowing = new DisableEditModel();
        modelFollowers = new DisableEditModel();
        modelOthers = new DisableEditModel();
        dateFormat = new SimpleDateFormat("HH:mm:ss");
        client = new MessageClient("rmi://127.0.0.1:2045/MessageServer");
        client.addObserver(new RemoteObserver());
    }

    @Override
    public void run() {
        setBorder(jspMessage);
        setBorder(txtUrl);
        setBorder(jspMessages);
        setBorder(tblFollowers);
        setBorder(tblFollowing);
        setBorder(tblOthers);
        setBorder(tabPane);

        spBase.setBorder(BorderFactory.createEmptyBorder());
        spLeft.setBorder(BorderFactory.createEmptyBorder());
        txtMessages.setBorder(BorderFactory.createEmptyBorder());
        txtMessage.setBorder(BorderFactory.createEmptyBorder());

        tblFollowers.setModel(modelFollowers);
        tblFollowing.setModel(modelFollowing);
        tblOthers.setModel(modelOthers);

        tblFollowers.setSelectionMode(SINGLE_SELECTION);
        tblFollowing.setSelectionMode(SINGLE_SELECTION);
        tblOthers.setSelectionMode(SINGLE_SELECTION);

        tblFollowers.setAutoCreateColumnsFromModel(true);
        tblFollowing.setAutoCreateColumnsFromModel(true);
        tblOthers.setAutoCreateColumnsFromModel(true);

        modelFollowers.addColumn("Name");
        modelFollowing.addColumn("Name");
        modelOthers.addColumn("Name");

        FollowController followController = new FollowController();
        btnFollow.addActionListener(followController);
        btnLogin.addActionListener(new LoginController());
        btnRegister.addActionListener(new RegisterController());
        btnSend.addActionListener(new SendController());
        btnUnfollow.addActionListener(new UnfollowController());
        btnFollowOthers.addActionListener(followController);
        btnGetMessages.addActionListener(new GetController());

        DefaultCaret caret = (DefaultCaret)txtMessages.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

        btnLogout.setEnabled(false);
        btnSend.setEnabled(false);
        btnGetMessages.setEnabled(false);
        btnFollow.setEnabled(false);
        btnUnfollow.setEnabled(false);
        btnFollowOthers.setEnabled(false);

        txtMessage.addKeyListener(new MessageKeyListener());

        frame = new JFrame();
        frame.setContentPane(panel);
        frame.setResizable(false);
        //To change body of implemented methods use File | Settings | File Templates.
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(700, 400);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private void setBorder(JComponent component) {
        component.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY),
                BorderFactory.createEmptyBorder(3, 3, 3, 3)));
    }

    private String log(String text) {
        return String.format("[%s] %s\n", dateFormat.format(new Date()), text);
    }

    private void refreshModel(DefaultTableModel model, Iterable<IUser> users) {
        model.setRowCount(0);
        for (IUser user : users)
            model.addRow(new IUser[] {user});
    }

    private void refreshMessages(Iterable<Message> messages) {
        for (Message message : messages) {
            txtMessages.append(String.format("\n%s%s\n", log(message.getNickname()), message.getMessage()));
        }

    }


    // nested classes

    private class LoginController implements ActionListener, ILoginListener {
        LoginForm loginForm;
        @Override
        public void actionPerformed(ActionEvent e) {
            loginForm = new LoginForm(this);
            loginForm.pack();
            loginForm.setLocationRelativeTo(panel);
            loginForm.setVisible(true);
        }

        @Override
        public void okPressed(String login, char[] password) {
            client.setUrl(txtUrl.getText());

            try {
                if (client.login(login,  new String(password))) {
                    refreshModel(modelOthers, client.getOthers());
                    refreshModel(modelFollowers, client.getFollowers());
                    refreshModel(modelFollowing, client.getFollowing());
                    btnSend.setEnabled(true);
                    btnGetMessages.setEnabled(true);
                    btnFollow.setEnabled(true);
                    btnUnfollow.setEnabled(true);
                    btnFollowOthers.setEnabled(true);
                    btnLogout.setEnabled(true);
                    btnRegister.setEnabled(false);
                    btnLogin.setEnabled(false);
                }
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(loginForm, e.getMessage(), "Message Client", JOptionPane.WARNING_MESSAGE);
            }

            for (int i = 0; i < password.length; i++)
                password[i] = 0;
        }
    }

    private class RegisterController implements ActionListener, ILoginListener {
        LoginForm loginForm;
        @Override
        public void actionPerformed(ActionEvent e) {
            loginForm = new LoginForm(this);
            loginForm.pack();
            loginForm.setLocationRelativeTo(panel);
            loginForm.setVisible(true);
        }

        @Override
        public void okPressed(String login, char[] password) {
            client.setUrl(txtUrl.getText());
            try {
                if (client.register(login, new String(password))) {
                    refreshModel(modelOthers, client.getOthers());
                    refreshModel(modelFollowers, client.getFollowers());
                    refreshModel(modelFollowing, client.getFollowing());
                    btnSend.setEnabled(true);
                    btnGetMessages.setEnabled(true);
                    btnFollow.setEnabled(true);
                    btnUnfollow.setEnabled(true);
                    btnFollowOthers.setEnabled(true);
                    btnLogout.setEnabled(true);
                    btnRegister.setEnabled(false);
                    btnLogin.setEnabled(false);
                }
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(loginForm, e.getMessage(), "Message Client", JOptionPane.WARNING_MESSAGE);
            }

            for (int i = 0; i < password.length; i++)
                password[i] = 0;
        }
    }

    private class SendController implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            client.postMessage(txtMessage.getText());
            txtMessage.setText("");
        }
    }

    private class GetController implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            refreshMessages(client.getMessages());
        }
    }


    private class FollowController implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int tab = tabPane.getSelectedIndex();
            if (tab == 1) {
                followFrom(modelFollowers, tblFollowers.getSelectedRow());
            }
            else {
                followFrom(modelOthers, tblOthers.getSelectedRow());
                if (tblOthers.getSelectedRow() != -1)
                    modelOthers.removeRow(tblOthers.getSelectedRow());
            }
        }

        private void followFrom(DefaultTableModel model, int row) {
            if (row == -1)
                return;

            IUser user = (IUser) model.getValueAt(row, 0);
            if (client.follow(user))  {
                modelFollowing.addRow(new IUser[] { user });
            }
        }
    }

    private class UnfollowController implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int row = tblFollowing.getSelectedRow();
            if (row != -1 ) {
                IUser user = (IUser) modelFollowing.getValueAt(row, 0);
                if (client.unfollow(user)) {
                    modelFollowing.removeRow(row);
                    if (!client.isFollowingMe(user) && !modelOthers.getDataVector().contains(user))
                        modelOthers.addRow(new IUser[] { user });
                }
            }
        }
    }

    private class RemoteObserver implements Observer {

        public RemoteObserver() {
        }
        @Override
        public void update(Observable o, Object arg) {
            RemoteStateChanged event = (RemoteStateChanged)arg;

            if (event.getState() == ClientListenerState.MESSAGE)
                handleMessages();
            else if (event.getState() == ClientListenerState.UNFOLLOW)
                handleUnfollow((String) event.getData());
            else if (event.getState() == ClientListenerState.FOLLOW)
                handleFollow((String)event.getData());
        }

        private void handleFollow(String nickname) {
            // FIXME: lists should be handled in the client
            txtMessages.append(log(String.format("%s is %s you!", nickname, "following")));
            refreshModel(modelFollowers, client.getFollowers());
            refreshModel(modelOthers, client.getOthers());
        }

        private void handleUnfollow(String nickname) {
            // FIXME: lists should be handled in the client
            txtMessages.append(log(String.format("%s stopped %s you!", nickname, "following")));
            refreshModel(modelFollowers, client.getFollowers());
            refreshModel(modelOthers, client.getOthers());
        }

        private void handleMessages() {
            txtMessages.append(log("There are new message on the server!"));
        }

    }

    private class DisableEditModel extends DefaultTableModel {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }


    private class MessageKeyListener implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
            JTextArea component = (JTextArea)e.getComponent();
            if (component.getText().length() >= 140)
                e.consume();
        }

        @Override
        public void keyPressed(KeyEvent e) {
            JTextArea component = (JTextArea)e.getComponent();
            lblNotification.setText(Integer.toString(140 - component.getText().length()));
        }

        @Override
        public void keyReleased(KeyEvent e) {
            keyPressed(e);
        }
    }

    class GlobalExceptionHandler implements Thread.UncaughtExceptionHandler {
        public void uncaughtException(Thread thread, Throwable exception) {
            JOptionPane.showMessageDialog(frame, exception.getMessage());
        }
    }
}
