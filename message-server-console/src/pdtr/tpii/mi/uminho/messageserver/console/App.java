package pdtr.tpii.mi.uminho.messageserver.console;

import pdtr.tpii.ers.mi.uminho.messagecontract.IGatekeeper;
import pdtr.tpii.ers.mi.uminho.messagecontract.IMessageServer;
import pdtr.tpii.ers.mi.uminho.messageserver.Gatekeeper;
import pdtr.tpii.ers.mi.uminho.messageserver.MessageServer;
import pdtr.tpii.mi.uminho.messageserver.console.ServerEventListener;

import javax.swing.*;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 28/03/2011
 * Time: 19:37
 * To change this template use File | Settings | File Templates.
 */
public class App {
    public static void main(String[] args) {
        MessageServer server = new MessageServer();
        try {
            server.start();
            server.addEventListener(new ServerEventListener());
            System.out.println("Server Online!");
        } catch (RemoteException e) {
            e.printStackTrace();
            System.out.println("Trouble starting up the server, check console log!");
        }
    }
}
