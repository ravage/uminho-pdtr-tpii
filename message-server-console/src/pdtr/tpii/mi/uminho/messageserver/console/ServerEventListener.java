package pdtr.tpii.mi.uminho.messageserver.console;

import pdtr.tpii.ers.mi.uminho.messageserver.IListener;
import pdtr.tpii.ers.mi.uminho.messagecontract.IUser;
import pdtr.tpii.ers.mi.uminho.messagecontract.User;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 02/04/2011
 * Time: 14:50
 * To change this template use File | Settings | File Templates.
 */
public class ServerEventListener implements IListener {
    private final SimpleDateFormat dateFormat;

    public ServerEventListener() {
        dateFormat = new SimpleDateFormat("dd/MM/yyyy @ HH:mm:ss");
    }


    private String log(String command, String host, String message) {
        return String.format("[%s][%s](%s): %s", dateFormat.format(new Date()), command, host, message);
    }

    private String log(String command, String host) {
        return log(command, host, "");
    }

    @Override
    public void userRegistered(String nickname, String host, boolean state) {
        System.out.println(log("Register", host,  String.format("%s:%s", nickname, getLiteralResult(state))));
    }

    @Override
    public void followRequest(IUser requester, IUser user) {
        System.out.println(log("Follow", requester.getHost(), String.format("%s -> %s", requester.getName(), user.getName())));
    }

    @Override
    public void unfollowRequest(IUser requester, IUser user) {
        System.out.println(log("Unfollow", requester.getHost(), String.format("%s -> %s", requester.getName(), user.getName())));
    }

    @Override
    public void messagePosted(IUser requester, String message) {
       System.out.println(log("Message", requester.getHost(), requester.getName()));
    }

    @Override
    public void userLoggedIn(String nickname, String host, boolean result) {
        System.out.println(log("Login", host, String.format("%s:%s", nickname, getLiteralResult(result))));
    }

    @Override
    public void userListRequest(IUser issuer) {
        System.out.println(log("UserList", issuer.getHost(), issuer.getName()));
    }

    @Override
    public void getMessagesRequest(IUser issuer) {
        System.out.println(log("Read", issuer.getHost(), issuer.getName()));
    }

    @Override
    public void pingNotification(User issuer) {
        System.out.println(log("Ping", issuer.getHost(), issuer.getName()));
    }

    private String getLiteralResult(boolean status) {
        return (status) ? "Success" : "Fail";
    }
}
